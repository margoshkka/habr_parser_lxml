import lxml.html as html
from urllib.request import urlopen
import json


def decompose(el):
    return el.xpath('/*')


def main():
    output = []

    main_domain_stat = 'https://habr.com/'
    page = html.parse(urlopen(main_domain_stat)).getroot()

    articles_links = page.xpath(
       '//li/article[contains(@class, "post")]/h2[@class="post__title"]/a[@class="post__title_link"]/@href'
    )

    for link in articles_links:
        page = html.parse(urlopen(link)).getroot()
        header = page.xpath(
          '//article[contains(@class, "post")]//h1/span/text()'
        )

        post_body = page.xpath(
          '//article[contains(@class, "post")]//div[contains(@class, "post__body post__body_full")]'
          '//div[contains(@class, "post__text")]/.'
        )

        text = post_body[0].xpath(
            './text() | ./p/text() | ./i/text() | ./b/text()'
        )
        image = post_body[0].xpath(
           './img/@src'
        )

        output.append({
           'link': link,
           'header': header[0],
           'text': ''.join(text),
           'images': image
        })

    with open('parsed.json', 'w', encoding='utf-8') as json_file:
        json.dump(output, json_file, sort_keys=True, indent=4, ensure_ascii=False)
    json_file.close()


if __name__ == '__main__':
    main()

